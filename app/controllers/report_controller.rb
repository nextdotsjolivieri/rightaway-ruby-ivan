class ReportController < ApplicationController
  def index

    if validate_date(params[:from_date]) and validate_date(params[:to_date])
      @premium_subscription = PremiumSubscription.between(params[:from_date], params[:to_date]).order(status: :desc)
    else
      @premium_subscription = PremiumSubscription.all.order(status: :desc)
    end

    respond_to do |format|
      format.html 
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="report.xlsx"'
      }
    end
  end

  def update
    premium_subscription = PremiumSubscription.find(params[:id])
    premium_subscription[:status] = 1 - premium_subscription[:status]

    respond_to do |format|
      if premium_subscription.save
        format.json { render json: {subscription_status: premium_subscription.status}, status: :created}
      else
        format.json { render json: premium_subscription.errors, status: :unprocessable_entity }
      end
    end
  end

private
  
  def validate_date(date)
    date.present? and /\d{4}\-\d{2}\-\d{2}/.match(date).present?
  end

end
