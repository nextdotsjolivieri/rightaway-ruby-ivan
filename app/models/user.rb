# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  username   :string
#  uid        :string
#  provider   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ActiveRecord::Base
  # forgot password field.
  validates :first_name, :username, presence: true

  has_one :premium_subscription, :dependent => :destroy

  # this method return true if user is subscribed, and false otherwise.
  def subscribed
    premium_subscription.present?
  end

end
