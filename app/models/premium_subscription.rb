# == Schema Information
#
# Table name: premium_subscriptions
#
#  id                  :integer          not null, primary key
#  shirt_size          :integer
#  user_id             :integer
#  subscription_length :integer
#  status              :integer          default(0)
#  shipping_addres     :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class PremiumSubscription < ActiveRecord::Base
  validates :shirt_size, :user, :subscription_length, presence: true
  belongs_to :user

  scope :between, ->(from, to) { where("created_at >= ?", from.to_date.beginning_of_day).where("created_at <= ?", to.to_date.end_of_day) }

  enum shirt_size: ["s", "m", "l", "xl", "xxl"]
  def shirt_size_enum
    self.class.shirt_sizes.to_a
  end

  enum subscription_length: ["1 week", "1 month", "1 year"]
  def subscription_length_enum
    self.class.subscription_lengths.to_a
  end

  enum status: ["unshipped", "shipped"]
  def status_enum
    self.class.statuses.to_a
  end

end
