class CreatePremiumSubscriptions < ActiveRecord::Migration
  def change
    create_table :premium_subscriptions do |t|
      t.integer :shirt_size, limit: 2
      t.integer :user_id
      t.integer :subscription_length, limit: 1
      t.integer :status, limit: 1, default: 0
      t.text :shipping_addres

      t.timestamps null: false
    end
  end
end
