# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Create 20 users
(1..20).each do |item|
  user = User.new({first_name: Faker::Name.first_name, last_name: Faker::Name.last_name ,username: "#{Faker::Name.first_name.downcase}_#{item}"})
  if user.valid?
    user.save
    
    # Create premium Subscription in a rage of ten days
    subscription = PremiumSubscription.new(shirt_size: [0,1,2,3,4].sample, user: user, subscription_length: [0,1,2].sample, status: (0..1).to_a.sample, created_at: Time.now + (-5..5).to_a.sample.days)
    if subscription.valid?
      subscription.save
    end

  end
end
