# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  username   :string
#  uid        :string
#  provider   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(first_name: 'Ivan', username: 'nextdot')
    @user.save
  end

  test "should not save user without username and a first_name" do
    user = User.new
    assert_not user.save, "Saved the user without a username and first_name"
  end

  test "should not save the user subscribed without being subscribed" do
    subscribed = @user.subscribed
    assert_not subscribed, "Saved the user as subscribed without being subscribed"
  end

end
