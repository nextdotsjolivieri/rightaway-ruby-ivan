# == Schema Information
#
# Table name: premium_subscriptions
#
#  id                  :integer          not null, primary key
#  shirt_size          :integer
#  user_id             :integer
#  subscription_length :integer
#  status              :integer          default(0)
#  shipping_addres     :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'test_helper'

class PremiumSubscriptionTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(first_name: 'Ivan', username: 'nextdot')
    @user.save
  end

  test 'valid test user' do
    assert @user.valid?, "Invalid test User"
  end

  test "should not save subscription without shirt_size, user_id and subscription_length" do
    subscription = PremiumSubscription.new
    assert_not subscription.save, "Saved the subscription without a shirt_size, user_id and subscription_length"
  end

  test "should not save subscription with a shirt_size bigger than 4" do
    subscription = PremiumSubscription.new(shirt_size: 5, user_id: @user, subscription_length: 2)
    assert_not subscription.save, "Saved the subscription with a shirt_size bigger than 4"
  end

  test "should not save subscription with a subscription_length bigger than 2" do
    subscription = PremiumSubscription.new(shirt_size: 0, user_id: @user, subscription_length: 3)
    assert_not subscription.save, "Saved the subscription with a subscription_length bigger than 2"
  end

  test "should not save subscription with a status other than 1 or 0" do
    subscription = PremiumSubscription.new(shirt_size: 4, user_id: @user, subscription_length: 2, status: 3)
    assert_not subscription.save, "Saved the subscription with a status other than 1 or 0"
  end

  test "should save subscription with a real user" do
    subscription = PremiumSubscription.new(shirt_size: 1, user_id: @user.id, subscription_length: 2)
    assert subscription.save, "Not save the subscription with a real user"
  end

  test "should not save subscription without an existing user" do
    subscription = PremiumSubscription.new(shirt_size: 1, user_id: 10, subscription_length: 2)
    assert_not subscription.save, "Saved the subscription without an existing user"
  end
end
